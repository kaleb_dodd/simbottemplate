package org.simbotics.frcTemplate;

public class SystemCheckConstants {

	//////// DRIVE ////////
	
	public static final double DRIVE_CURRENT_MIN = 10;
	public static final double DRIVE_CURRENT_MAX = 15;
	public static final double DRIVE_CURRENT_EPS = 2.5;
	public static final double DRIVE_FPS_MIN = 8;
	public static final double DRIVE_FPS_MAX = 11;
	public static final double DRIVE_FPS_EPS = 0.5;
	
	
	//////// INTAKE ////////
	public static final double INTAKE_CURRENT_MIN = 6;
	public static final double INTAKE_CURRENT_MAX = 11;
	public static final double INTAKE_CURRENT_EPS = 2;
	
	
	
	//////// ELEVATOR ////////
	public static final double ELEVATOR_CURRENT_MIN = 10;
	public static final double ELEVATOR_CURRENT_MAX = 15;
	public static final double ELEVATOR_CURRENT_EPS = 2.5;
	public static final double ELEVATOR_FPS_MIN = 0.3;
	public static final double ELEVATOR_FPS_MAX = 6;
	public static final double ELEVATOR_FPS_EPS = 0.5;
	
	
	
	//////// WRIST //////// 
	public static final double WRIST_CURRENT_MIN = 10;
	public static final double WRIST_CURRENT_MAX = 15;
	public static final double WRIST_DPS_MIN = 90;
	public static final double WRIST_DPS_MAX = 120;

	
	
	
}
