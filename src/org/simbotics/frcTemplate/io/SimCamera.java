package org.simbotics.frcTemplate.io;

import org.opencv.core.Mat;

import edu.wpi.cscore.CvSink;
import edu.wpi.cscore.CvSource;
import edu.wpi.cscore.UsbCamera;
import edu.wpi.cscore.VideoException;
import edu.wpi.first.wpilibj.CameraServer;

public class SimCamera implements Runnable {

	private UsbCamera camera;

	private CvSink cvSink;
	private CvSource outputStream;

	private boolean initialized = false;

	private Mat source;

	public SimCamera() {
		this.source = new Mat();
	}

	public void init() {

		try {
			this.camera = new UsbCamera("usb cam", "/dev/video0");
		} catch (Exception e) {
			e.printStackTrace();
		}

		CameraServer.getInstance().addCamera(camera);
		this.camera.setResolution(320, 240);
		this.camera.setFPS(120);

		this.cvSink = CameraServer.getInstance().getVideo();
		this.outputStream = CameraServer.getInstance().putVideo("cam0", 320, 240);
		this.initialized = true;
	}

	@Override
	public void run() {
		while (!Thread.interrupted()) { // thread is always running

			if (this.camera == null && this.initialized == false) {
				try {
					this.init();
				} catch (VideoException ve) {
					ve.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (this.camera.isConnected() && this.initialized == true) {
				if (CameraServer.getInstance() != null) {
					this.cvSink.grabFrameNoTimeout(this.source);
					this.outputStream.putFrame(source);
				}
			}
		}
	}
}
