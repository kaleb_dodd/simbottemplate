package org.simbotics.frcTemplate.io;

import org.simbotics.frcTemplate.RobotConstants;
import org.simbotics.frcTemplate.auton.mode.AutonMode;
import org.simbotics.frcTemplate.util.PIDConstants;
import org.simbotics.frcTemplate.util.ProfileConstants;
import org.simbotics.frcTemplate.util.TrajectoryConfig;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Dashboard {
	private static Dashboard instance;

	private Dashboard() {

		SmartDashboard.putNumber("OP TURN VAL", RobotConstants.OP_TURN_RATE);

	}

	public static Dashboard getInstance() {
		if (instance == null) {
			instance = new Dashboard();
		}
		return instance;
	}

	public void updateAll() {
		updateSensorDisplay();
	}


	public void updateSensorDisplay() {
		SensorInput sensorInput = SensorInput.getInstance();
		RobotOutput robotOut = RobotOutput.getInstance();
		SmartDashboard.putNumber("123_Gyro", sensorInput.getGyroAngle());
		SmartDashboard.putString("123_ALLIANCE", sensorInput.getAllianceColour().toString());
		SmartDashboard.putString("123_MODE: ", sensorInput.getDriverStationMode().toString());
		SmartDashboard.putNumber("123_MATCH TIME: ", sensorInput.getMatchTime());
		SmartDashboard.putNumber("123_X Position: ", sensorInput.getDriveXPos());
		SmartDashboard.putNumber("123_Y Position: ", sensorInput.getDriveYPos());
		SmartDashboard.putNumber("123_Left Encoder: ", sensorInput.getEncoderLeft());
		SmartDashboard.putNumber("123_Right Encoder: ", sensorInput.getEncoderRight());
		SmartDashboard.putNumber("123_Drive Speed FPS: ", sensorInput.getDriveSpeedFPS());

	}

	public void updateAutoModes(AutonMode mode) {
		SmartDashboard.putString("1_Auto", className(mode));
		
	}

	public void printAutoModes(AutonMode mode) {
		System.out.println(" ");
		System.out.println("1_Auto: " + className(mode));
	}

	public void updateAutoDelay(double delay) {
		SmartDashboard.putNumber("1_Auton Delay: ", delay);
	}

	public void printAutoDelay(double delay) {
		System.out.println("1_Auton Delay: " + delay);
	}

	public double getConstant(String name, double defaultValue) {
		return SmartDashboard.getNumber(name, defaultValue);
	}

	public PIDConstants getPIDConstants(String name, PIDConstants constants) {
		double p = SmartDashboard.getNumber("5_" + name + " - P Value", constants.p);
		double i = SmartDashboard.getNumber("5_" + name + " - I Value", constants.i);
		double d = SmartDashboard.getNumber("5_" + name + " - D Value", constants.d);
		double ff = SmartDashboard.getNumber("5_" + name + " - FF Value", constants.ff);
		double eps = SmartDashboard.getNumber("5_" + name + " - EPS Value", constants.eps);
		return new PIDConstants(p, i, d, ff,eps);
	}

	public void putPIDConstants(String name, PIDConstants constants) {
		SmartDashboard.putNumber("5_" + name + " - P Value", constants.p);
		SmartDashboard.putNumber("5_" + name + " - I Value", constants.i);
		SmartDashboard.putNumber("5_" + name + " - D Value", constants.d);
		SmartDashboard.putNumber("5_" + name + " - FF Value", constants.ff);
		SmartDashboard.putNumber("5_" + name + " - EPS Value", constants.eps);
	}

	public ProfileConstants getProfileConstants(String name, ProfileConstants constants) {
		double p = SmartDashboard.getNumber("5_" + name + " - P Value", constants.p);
		double i = SmartDashboard.getNumber("5_" + name + " - I Value", constants.i);
		double d = SmartDashboard.getNumber("5_" + name + " - D Value", constants.d);
		double vFF = SmartDashboard.getNumber("3_" + name + " - vFF Value", constants.vFF);
		double aFF = SmartDashboard.getNumber("3_" + name + " - aFF Value", constants.aFF);
		double dFF = SmartDashboard.getNumber("3_" + name + " - dFF Value", constants.dFF);
		double gFF = SmartDashboard.getNumber("3_" + name + " - gFF Value", constants.gravityFF);
		double posEps = SmartDashboard.getNumber("3_" + name + " - Pos EPS Value", constants.positionEps);
		double velEps = SmartDashboard.getNumber("3_" + name + " - Vel EPS Value", constants.velocityEps);
		return new ProfileConstants(p, i, d, vFF, aFF, dFF, gFF, posEps, velEps);
	}

	public void putProfileConstants(String name, ProfileConstants constants) {
		SmartDashboard.putNumber("5_" + name + " - P Value", constants.p);
		SmartDashboard.putNumber("5_" + name + " - I Value", constants.i);
		SmartDashboard.putNumber("5_" + name + " - D Value", constants.d);
		SmartDashboard.putNumber("3_" + name + " - vFF Value", constants.vFF);
		SmartDashboard.putNumber("3_" + name + " - aFF Value", constants.aFF);
		SmartDashboard.putNumber("3_" + name + " - dFF Value", constants.dFF);
		SmartDashboard.putNumber("3_" + name + " - gFF Value", constants.gravityFF);
		SmartDashboard.putNumber("3_" + name + " - Pos EPS Value", constants.positionEps);
		SmartDashboard.putNumber("3_" + name + " - Vel EPS Value", constants.velocityEps);
	}

	public TrajectoryConfig getTrajectoryConfig(String name, TrajectoryConfig constants) {
		double maxAccel = SmartDashboard.getNumber("3_" + name + " - Max Accel Value", constants.maxAcceleration);
		double maxDecel = SmartDashboard.getNumber("3_" + name + " - Max Decel Value", constants.maxDeceleration);
		double maxVel = SmartDashboard.getNumber("3_" + name + " - Max Vel Value", constants.maxVelocity);
		return new TrajectoryConfig(maxAccel, maxDecel, maxVel);

	}
	
	
	public double getOPTurnRate() {
		return SmartDashboard.getNumber("OP TURN VAL", RobotConstants.OP_TURN_RATE);
	}

	public void putTrajectoryConfig(String name, TrajectoryConfig constants) {
		SmartDashboard.putNumber("3_" + name + " - Max Accel Value", constants.maxAcceleration);
		SmartDashboard.putNumber("3_" + name + " - Max Decel Value", constants.maxDeceleration);
		SmartDashboard.putNumber("3_" + name + " - Max Vel Value", constants.maxVelocity);

	}

	private String className(Object obj) {
		return obj.getClass().getSimpleName();
	}
}
