package org.simbotics.frcTemplate.io;

import org.simbotics.frcTemplate.RobotConstants;
import org.simbotics.frcTemplate.util.PIDConstants;

import com.ctre.phoenix.motorcontrol.ControlFrame;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;


public class RobotOutput {
	private static RobotOutput instance;
	
	private TalonSRX driveLeft1;
	private VictorSPX driveLeft2;
	private VictorSPX driveLeft3;

	private TalonSRX driveRight1;
	private VictorSPX driveRight2;
	private VictorSPX driveRight3;

	
	private RobotOutput() {
		
		this.driveLeft1 = new TalonSRX(0);
		this.driveLeft2 = new VictorSPX(1);
		this.driveLeft3 = new VictorSPX(2);

		this.driveRight1 = new TalonSRX(3);
		this.driveRight2 = new VictorSPX(4);
		this.driveRight3 = new VictorSPX(5);

		this.configureSpeedControllers();
	}

	public static RobotOutput getInstance() {
		if (instance == null) {
			instance = new RobotOutput();
		}
		return instance;

	}

	// Motor Commands

	public void configureSpeedControllers() {

		this.driveRight1.setControlFramePeriod(ControlFrame.Control_3_General, 20);
		this.driveRight2.setControlFramePeriod(ControlFrame.Control_3_General, 20);
		this.driveRight3.setControlFramePeriod(ControlFrame.Control_3_General, 20);
		this.driveLeft1.setControlFramePeriod(ControlFrame.Control_3_General, 20);
		this.driveLeft2.setControlFramePeriod(ControlFrame.Control_3_General, 20);
		this.driveLeft3.setControlFramePeriod(ControlFrame.Control_3_General, 20);

		this.driveLeft1.setInverted(true);
		this.driveLeft2.setInverted(true);
		this.driveLeft3.setInverted(true);

		this.driveRight1.setInverted(false);
		this.driveRight2.setInverted(false);
		this.driveRight3.setInverted(false);

		this.driveLeft2.follow(this.driveLeft1);
		this.driveLeft3.follow(this.driveLeft1);

		this.driveRight2.follow(this.driveRight1);
		this.driveRight3.follow(this.driveRight1);

		this.driveLeft1.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 10);
		this.driveRight1.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 10);

		
	}

	// Drive
	public void setDriveLeft(double output) {
		this.driveLeft1.set(ControlMode.PercentOutput, output);
	}

	public void setDriveRight(double output) {
		this.driveRight1.set(ControlMode.PercentOutput, output);
	}
	
	public void setDriveTest(double output, int motor) {
		switch (motor) {
		case 0:
			this.driveLeft1.set(ControlMode.PercentOutput, output);
			this.driveLeft2.set(ControlMode.PercentOutput, 0);
			this.driveLeft3.set(ControlMode.PercentOutput, 0);
			
		case 1:
			this.driveLeft1.set(ControlMode.PercentOutput, 0);
			this.driveLeft2.set(ControlMode.PercentOutput, output);
			this.driveLeft3.set(ControlMode.PercentOutput, 0);
		case 2:
			this.driveLeft1.set(ControlMode.PercentOutput, 0);
			this.driveLeft2.set(ControlMode.PercentOutput, 0);
			this.driveLeft3.set(ControlMode.PercentOutput, output);
		case 3:
			this.driveRight1.set(ControlMode.PercentOutput, output);
			this.driveRight2.set(ControlMode.PercentOutput, 0);
			this.driveRight3.set(ControlMode.PercentOutput, 0);
		case 4:
			this.driveRight1.set(ControlMode.PercentOutput, 0);
			this.driveRight2.set(ControlMode.PercentOutput, output);
			this.driveRight3.set(ControlMode.PercentOutput, 0);
		case 5:
			this.driveRight1.set(ControlMode.PercentOutput, 0);
			this.driveRight2.set(ControlMode.PercentOutput, 0);
			this.driveRight3.set(ControlMode.PercentOutput, output);
			
		}
	}
	

	public void setDriveLeftVelocity(double output) {
		output = (output * (RobotConstants.DRIVE_TICKS_PER_INCH_HIGH * 12)) / 10; //output in ticks/100ms
		this.driveLeft1.set(ControlMode.Velocity, output);
	}

	public void setDriveRightVelocity(double output) {
		output = (output * (RobotConstants.DRIVE_TICKS_PER_INCH_HIGH * 12)) / 10; //output in ticks/100ms
		this.driveRight1.set(ControlMode.Velocity, output);
	}

	public void resetDriveEncoders() {
		this.driveLeft1.setSelectedSensorPosition(0, 0, 0);
		this.driveRight1.setSelectedSensorPosition(0, 0, 0);
	}

	public int getDriveLeftSpeed() {
		return this.driveLeft1.getSelectedSensorVelocity(0);
	}

	public int getDriveRightSpeed() {
		return this.driveRight1.getSelectedSensorVelocity(0);
	}

	public int getDriveRightEnc() {
		return this.driveRight1.getSelectedSensorPosition(0);
	}

	public int getDriveLeftEnc() {
		return this.driveLeft1.getSelectedSensorPosition(0);
	}
	
	public void configureDrivePID(PIDConstants constants) {
		this.driveRight1.selectProfileSlot(0, 0);
		this.driveRight1.config_kP(0, constants.p, 20);
		this.driveRight1.config_kI(0, constants.i, 20);
		this.driveRight1.config_kD(0, constants.d, 20);
		this.driveRight1.configAllowableClosedloopError(0, (int) constants.eps, 20);
		this.driveRight1.config_IntegralZone(0, 4096, 20);
		this.driveRight1.config_kF(0, constants.ff, 20);
		
		this.driveLeft1.selectProfileSlot(0, 0);
		this.driveLeft1.config_kP(0, constants.p, 20);
		this.driveLeft1.config_kI(0, constants.i, 20);
		this.driveLeft1.config_kD(0, constants.d, 20);
		this.driveLeft1.configAllowableClosedloopError(0, (int) constants.eps, 20);
		this.driveLeft1.config_IntegralZone(0, 4096, 20);
		this.driveLeft1.config_kF(0, constants.ff, 20);
		
	}

	
	public void stopAll() {
		setDriveLeft(0);
		setDriveRight(0);
		
		// shut off things here
	}

}
