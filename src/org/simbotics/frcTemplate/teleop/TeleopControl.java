package org.simbotics.frcTemplate.teleop;

import java.util.ArrayList;

/**
 *
 * @author Michael
 */
public class TeleopControl {

	private ArrayList<TeleopComponent> components;
	private static TeleopControl instance;
	private boolean failure = false;
	private boolean firstCycleDone = true;
	private boolean[] currentResults; 
	private int subSystemIndex = 0;
	private int subSystemCount;
	
	
	public static TeleopControl getInstance() {
		if (instance == null) {
			instance = new TeleopControl();
		}
		return instance;
	}

	private TeleopControl() {
		// GOTCHA: remember to add teleop components here!
		this.components = new ArrayList<>();

		// add teleop components here
		this.components.add(TeleopDrive.getInstance());
		
		this.subSystemCount = this.components.size();

	}
	
	
	public void runChecks() {
		if(this.subSystemIndex < this.subSystemCount - 1) {
			this.currentResults = (this.components.get(this.subSystemIndex).systemCheck());
			
			if(this.currentResults[0]) { // is done
				this.failure &= this.currentResults[1]; 
				this.subSystemIndex++;
			} 
		} else { // we are done 
			if(this.firstCycleDone) {
				if(this.failure) {
					System.out.println("!!!!!!!! CHECK ABOVE TO SEE FAILURE !!!!!!!!");
				} else {
					System.out.println("ALL SYSTEMS GOOD :)");
				}
				this.firstCycleDone = false;
			}
		}
	}
	

	public void runCycle() {
		for (TeleopComponent t : this.components) {
			t.calculate();
		}
	}

	public void disable() {
		for (TeleopComponent t : this.components) {
			t.disable();
		}
	}

	public void initialize() {
		this.failure = false;
		this.firstCycleDone = true;
		this.subSystemIndex = 0;
		for (TeleopComponent t : this.components) {
			t.firstCycle();
		}
	}

}
