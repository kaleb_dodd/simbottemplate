package org.simbotics.frcTemplate.teleop;

/**
 *
 * @author jason
 */
public abstract class TeleopComponent {
	protected boolean[] checkResults = new boolean[] {false,false};
	
	protected boolean firstCycleChecking = true;
	
	protected long timeStartedChecking;
	
	public abstract void firstCycle();
	
	public abstract void calculate();

	public abstract void disable();
	
	public abstract boolean[] systemCheck();

	
}
