package org.simbotics.frcTemplate.teleop;

import org.simbotics.frcTemplate.SystemCheckConstants;
import org.simbotics.frcTemplate.io.DriverInput;
import org.simbotics.frcTemplate.io.RobotOutput;
import org.simbotics.frcTemplate.io.SensorInput;
import org.simbotics.frcTemplate.util.SimLib;

public class TeleopDrive extends TeleopComponent {

	private static TeleopDrive instance;

	private RobotOutput robotOut;
	private DriverInput driverIn;
	private SensorInput sensorIn;
	
	private double[] driveFPS = new double[6];
	private double[] driveCurrent = new double[6];

	public static TeleopDrive getInstance() {
		if (instance == null) {
			instance = new TeleopDrive();
		}
		return instance;
	}

	private TeleopDrive() {
		this.driverIn = DriverInput.getInstance();
		this.robotOut = RobotOutput.getInstance();
		this.sensorIn = SensorInput.getInstance();

	}

	@Override
	public void firstCycle() {
		// TODO Auto-generated method stub
		this.firstCycleChecking = true;

	}

	@Override
	public void calculate() {


		// driver inputs for drive
		double x;
		double y;

		// Deadzone Calculation
		if (Math.abs(this.driverIn.getDriverX()) < 0.03) {
			x = 0;
		} else {
			x = this.driverIn.getDriverX();
		}

		if (Math.abs(this.driverIn.getDriverY()) < 0.03) {
			y = 0;
		} else {
			y = this.driverIn.getDriverY();
		}

		
		
		
		this.robotOut.setDriveLeft(y + x);
		this.robotOut.setDriveRight(y - x);

	}

	@Override
	public void disable() {
		this.robotOut.setDriveLeft(0.0);
		this.robotOut.setDriveRight(0.0);
	}

	@Override
	public boolean[] systemCheck() {
		if(this.firstCycleChecking) {
			System.out.println("-------- CHECKING DRIVE --------");
			this.timeStartedChecking = System.currentTimeMillis();
			this.firstCycleChecking = false;
			this.checkResults[0] = false;
			this.checkResults[1] = false;
		}
		
		
		
		
		long time = System.currentTimeMillis() - this.timeStartedChecking;
	
		if(time < 1000) { // left 1 
			this.robotOut.setDriveTest(1.0, 0);
			this.driveCurrent[0] = this.sensorIn.getCurrent(0);
			this.driveFPS[0] = this.sensorIn.getDriveLeftFPS();
			
		} else if(time < 2000) { // left 2 
			this.robotOut.setDriveTest(1.0, 1);
			this.driveCurrent[1] = this.sensorIn.getCurrent(1);
			this.driveFPS[1] = this.sensorIn.getDriveLeftFPS();
			
		} else if(time < 3000) { // left 3
			this.robotOut.setDriveTest(1.0, 2);
			this.driveCurrent[2] = this.sensorIn.getCurrent(2);
			this.driveFPS[2] = this.sensorIn.getDriveLeftFPS();
			
		} else if(time < 4000) { // Right 1 
			this.robotOut.setDriveTest(1.0, 3);
			this.driveCurrent[3] = this.sensorIn.getCurrent(3);
			this.driveFPS[3] = this.sensorIn.getDriveRightFPS();
		} else if(time < 5000) { // Right 2 
			this.robotOut.setDriveTest(1.0, 4);
			this.driveCurrent[4] = this.sensorIn.getCurrent(4);
			this.driveFPS[4] = this.sensorIn.getDriveRightFPS();
		} else if(time < 6000) { // Right 3 
			this.robotOut.setDriveTest(1.0, 5);
			this.driveCurrent[5] = this.sensorIn.getCurrent(5);
			this.driveFPS[5] = this.sensorIn.getDriveRightFPS();
		} else {
			this.robotOut.setDriveTest(0,0);
			
			
			for(int i = 0; i < this.driveFPS.length; i++) {
				if(this.driveCurrent[i] < SystemCheckConstants.DRIVE_CURRENT_MIN) {
					System.out.println("!!!!!!!! DRIVE MOTOR " + i + " CURRENT LOW !!!!!!!!");
					System.out.println("!!!!!!!! DRIVE MOTOR " + i + " CURRENT =" + this.driveCurrent[i] + " !!!!!!!!");
					this.checkResults[1] = true;
				}
				
				if(this.driveCurrent[i] > SystemCheckConstants.DRIVE_CURRENT_MAX) {
					System.out.println("!!!!!!!! DRIVE MOTOR " + i + " CURRENT HIGH !!!!!!!!");
					System.out.println("!!!!!!!! DRIVE MOTOR " + i + " CURRENT =" + this.driveCurrent[i] + " !!!!!!!!");
					this.checkResults[1] = true;
				}
				
				if(this.driveFPS[i] < SystemCheckConstants.DRIVE_FPS_MIN) {
					System.out.println("!!!!!!!! DRIVE MOTOR " + i + " SPEED LOW !!!!!!!!");
					System.out.println("!!!!!!!! DRIVE MOTOR " + i + " FPS =" + this.driveFPS[i] + " !!!!!!!!");
					this.checkResults[1] = true;
				}
				
				if(this.driveFPS[i] > SystemCheckConstants.DRIVE_FPS_MAX) {
					System.out.println("!!!!!!!! DRIVE MOTOR " + i + " SPEED HIGH !!!!!!!!");
					System.out.println("!!!!!!!! DRIVE MOTOR " + i + " FPS =" + this.driveFPS[i] + " !!!!!!!!");
					this.checkResults[1] = true;
				}
				
			}
			
			if(!SimLib.allWithinRange(this.driveCurrent, this.driveCurrent[0], SystemCheckConstants.DRIVE_CURRENT_EPS)) {
				System.out.println("!!!!!!!! DRIVE CURRENTS DIFFERENT NOT WITHIN "
			+ SystemCheckConstants.DRIVE_CURRENT_EPS + " AMPS  !!!!!!!!");
				
				for(int i = 0; i < this.driveCurrent.length; i++) {
					System.out.println("!!!!!!!! DRIVE MOTOR " + i + " CURRENT =" + this.driveCurrent[i] + " !!!!!!!!");
				}
				this.checkResults[1] = true;
			}
			
			if(!SimLib.allWithinRange(this.driveFPS, this.driveFPS[0], SystemCheckConstants.DRIVE_FPS_EPS)) {
				System.out.println("!!!!!!!! DRIVE SPEEDS DIFFERENT NOT WITHIN " + SystemCheckConstants.DRIVE_FPS_EPS + " FPS  !!!!!!!!");
				for(int i = 0; i < this.driveFPS.length; i++) {
					System.out.println("!!!!!!!! DRIVE MOTOR " + i + " FPS =" + this.driveFPS[i] + " !!!!!!!!");
				}
				this.checkResults[1] = true;
			}
			
			
			this.checkResults[0] = true;
			
			
		}
		
		if(this.checkResults[0]) {
			if(this.checkResults[1]) {
				System.out.println("!!!!!!!! DRIVE FAILURE !!!!!!!!");
			} else {
				System.out.println("-------- DRIVE PASSED --------");
			}
			
		}
		
		return this.checkResults;
	}

}
