package org.simbotics.frcTemplate;

import org.simbotics.frcTemplate.io.Dashboard;
import org.simbotics.frcTemplate.util.PIDConstants;
import org.simbotics.frcTemplate.util.ProfileConstants;
import org.simbotics.frcTemplate.util.TrajectoryConfig;

public class RobotConstants {
	
	public static final boolean USING_DASHBOARD = true;
	private static Dashboard dashboard = Dashboard.getInstance();

 
	private static PIDConstants driveStraightPID = new PIDConstants(3.5, 0.08, 5, 0.25);
	private static PIDConstants driveTurnPID = new PIDConstants(0.15, 0.0015, 0.3, 0.25);
	private static PIDConstants talonVelocityPID = new PIDConstants(0.5, 0.0, 10, 0.16, 100);
	
	public static final double OP_TURN_RATE = 25; 

	// Drive Size Constants (Feet)
	public static final double DRIVE_WIDTH = 34.0 / 12.0;
	public static final double DRIVE_LENGTH = 39.125 / 12.0;
	public static final double DRIVE_TICKS_PER_INCH_HIGH = (4096.0) / (Math.PI * 4.23979) * (22 / 15.0); // wheel was 4.25

	
	public static PIDConstants getDriveStraightPID() {
		if (USING_DASHBOARD) {
			return dashboard.getPIDConstants("NEW_DRIVE_PID", driveStraightPID);
		} else {
			return driveStraightPID;
		}
	}
	
	public static PIDConstants getTalonVelocityPID() {
		if (USING_DASHBOARD) {
			return dashboard.getPIDConstants("TALON_VELOCITY_PID", talonVelocityPID);
		} else {
			return talonVelocityPID;
		}
	}
	
	public static PIDConstants getDriveTurnPID() {
		if (USING_DASHBOARD) {
			return dashboard.getPIDConstants("NEW_TURN_PID", driveTurnPID);
		} else {
			return driveTurnPID;
		}
	}


	public static void pushValues() {
		
		dashboard.putPIDConstants("NEW_DRIVE_PID", driveStraightPID);
		dashboard.putPIDConstants("NEW_TURN_PID", driveTurnPID);
		dashboard.putPIDConstants("TALON_VELOCITY_PID", talonVelocityPID);
		

	}
}
