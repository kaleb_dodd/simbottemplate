
package org.simbotics.frcTemplate;

import org.simbotics.frcTemplate.auton.AutonControl;
import org.simbotics.frcTemplate.io.Dashboard;
import org.simbotics.frcTemplate.io.DriverInput;
import org.simbotics.frcTemplate.io.Logger;
import org.simbotics.frcTemplate.io.RobotOutput;
import org.simbotics.frcTemplate.io.SensorInput;
import org.simbotics.frcTemplate.teleop.TeleopControl;
import org.simbotics.frcTemplate.util.Debugger;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;

public class Robot extends TimedRobot {
	private RobotOutput robotOut;
	private DriverInput driverInput;
	private SensorInput sensorInput;
	private TeleopControl teleopControl;
	private Logger logger;
	private Dashboard dashboard;
	private boolean pushToDashboard = true;
	

	@Override
	public void robotInit() {
		Debugger.defaultOn();
		this.dashboard = Dashboard.getInstance();
		if (this.pushToDashboard) {
			RobotConstants.pushValues();
		}
		this.robotOut = RobotOutput.getInstance();
		this.driverInput = DriverInput.getInstance();
		this.sensorInput = SensorInput.getInstance();
		this.teleopControl = TeleopControl.getInstance();
		this.logger = Logger.getInstance();

		LiveWindow.disableAllTelemetry();

	}

	@Override
	public void disabledInit() {
		this.robotOut.stopAll();
		this.teleopControl.disable();
		this.logger.close();
	}

	@Override
	public void disabledPeriodic() {
		this.sensorInput.update();
		this.dashboard.updateAll();
		AutonControl.getInstance().updateModes();

	}

	@Override
	public void autonomousInit() {
		AutonControl.getInstance().initialize();
		this.sensorInput.reset();
		this.sensorInput.resetAutonTimer();
		this.logger.openFile();
		this.robotOut.configureSpeedControllers();
		
	}

	@Override
	public void autonomousPeriodic() {
		this.sensorInput.update();
		this.dashboard.updateAll();
		AutonControl.getInstance().runCycle();
		this.logger.logAll();
	}
	
	public void testInit() {
		this.sensorInput.reset();
		this.teleopControl.initialize();
	}
	
	public void testPeriodic() {
		this.sensorInput.update();
		this.teleopControl.runChecks();
	}
	
	

	@Override
	public void teleopInit() {
		this.robotOut.configureSpeedControllers();
		this.teleopControl.initialize();
	}

	@Override
	public void teleopPeriodic() {
		this.sensorInput.update();
		this.dashboard.updateAll();
		this.teleopControl.runCycle();

	}

}
