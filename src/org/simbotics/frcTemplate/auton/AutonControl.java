package org.simbotics.frcTemplate.auton;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.simbotics.frcTemplate.auton.mode.AutonBuilder;
import org.simbotics.frcTemplate.auton.mode.AutonMode;
import org.simbotics.frcTemplate.auton.mode.DefaultMode;
import org.simbotics.frcTemplate.io.Dashboard;
import org.simbotics.frcTemplate.io.DriverInput;
import org.simbotics.frcTemplate.io.RobotOutput;
import org.simbotics.frcTemplate.util.Debugger;

/**
 *
 * @author Programmers
 */
public class AutonControl {

	private static AutonControl instance;

	public static final int NUM_ARRAY_MODE_STEPS = 4;

	private int autonDelay;
	private long autonStartTime;

	private boolean running;

	private int currentMode = 0;
	private AutonMode selectedMode; 
	
	private final AutonMode[] autonModes;

	private int currIndex;
	private AutonCommand[] commands;


	public static AutonControl getInstance() {
		if (instance == null) {
			instance = new AutonControl();
		}
		return instance;
	}

	private AutonControl() {
		this.autonDelay = 0;
		this.currIndex = 0;

		

		this.autonModes = new AutonMode[] {
				
				 new DefaultMode()
				
			
			};
		this.selectedMode = this.autonModes[0];
	}

	public void initialize() {
		Debugger.println("START AUTO");

		this.currIndex = 0;
		this.running = true;
		

		// initialize auton in runCycle
		AutonBuilder ab = new AutonBuilder();

		this.selectedMode.addToMode(ab);

		// get the full auton mode
		this.commands = ab.getAutonList();

		this.autonStartTime = System.currentTimeMillis();

		// clear out each components "run seat"
		AutonCommand.reset();
	}

	public void runCycle() {
		// haven't initialized list yet
		long timeElapsed = System.currentTimeMillis() - this.autonStartTime;
		if (timeElapsed > this.getAutonDelayLength() && this.running) {

			// start waiting commands
			while (this.currIndex < this.commands.length && this.commands[this.currIndex].checkAndRun()) {
				this.currIndex++;
			}
			// calculate call for all running commands
			AutonCommand.execute();
		} else {
			RobotOutput.getInstance().stopAll();
		}

	}

	public void stop() {
		this.running = false;
	}

	public long getAutonDelayLength() {
		return this.autonDelay * 500;
	}

	public void updateModes() {
		DriverInput driverIn = DriverInput.getInstance();

		boolean updatingAutoMode = false;

		try {

			int val = 0;
			if (driverIn.getAutonModeIncrease()) {
				val = 1;
			} else if (driverIn.getAutonModeIncreaseBy10()) {
				val = 10;
			} else if (driverIn.getAutonModeDecrease()) {
				val = -1;
			} else if (driverIn.getAutonModeDecreaseBy10()) {
				val = -10;
			}

			if (val != 0) {
				updatingAutoMode = true;

				this.currentMode = this.limit(this.currentMode + val, this.autonModes.length);
				
				this.selectedMode = this.autonModes[this.currentMode];
				

				Dashboard.getInstance().printAutoModes(this.selectedMode);

			} else if (driverIn.getAutonSetDelayButton()) {

				this.autonDelay = (int) ((driverIn.getAutonDelayStick() + 1) * 5.0);
				if (this.autonDelay < 0) {
					this.autonDelay = 0;
				}
				Dashboard.getInstance().printAutoDelay(this.autonDelay);
			}

		} catch (Exception e) {
			// this.autonMode = 0;
			// TODO: some kind of error catching

			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));

			
		}

		Dashboard.getInstance().updateAutoModes(this.selectedMode);

		// delay
		String delayAmt = "";
		if (this.autonDelay < 10) {
			// pad in a blank space for single digit delay
			delayAmt = " " + this.autonDelay;
		} else {
			delayAmt = "" + this.autonDelay;
		}

		Dashboard.getInstance().updateAutoDelay(this.autonDelay);
	}

	private int limit(int val, int limit) {
		if (val >= limit)
			return limit - 1;
		if (val < 0)
			return 0;
		return val;
	}
}
