package org.simbotics.frcTemplate.auton.util;

import org.simbotics.frcTemplate.auton.AutonCommand;
import org.simbotics.frcTemplate.auton.RobotComponent;
import org.simbotics.frcTemplate.util.Debugger;

public class TestPrint extends AutonCommand {

	private String toPrint;

	public TestPrint(String s) {
		super(RobotComponent.UTIL);
		this.toPrint = s;
	}

	@Override
	public void firstCycle() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean calculate() {
		Debugger.println(this.toPrint);
		return true;
	}

	@Override
	public void override() {
		// TODO Auto-generated method stub

	}

}
