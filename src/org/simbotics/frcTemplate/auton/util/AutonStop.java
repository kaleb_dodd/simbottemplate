package org.simbotics.frcTemplate.auton.util;

import org.simbotics.frcTemplate.auton.AutonCommand;
import org.simbotics.frcTemplate.auton.AutonControl;
import org.simbotics.frcTemplate.auton.RobotComponent;

/**
 *
 * @author Michael
 */
public class AutonStop extends AutonCommand {

	public AutonStop() {
		super(RobotComponent.UTIL);
	}

	@Override
	public void firstCycle() {
		// nothing
	}

	@Override
	public boolean calculate() {
		AutonControl.getInstance().stop();
		return true;
	}

	@Override
	public void override() {
		// nothing to do

	}

}
