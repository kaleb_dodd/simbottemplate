package org.simbotics.frcTemplate.auton.drive;

import org.simbotics.frcTemplate.auton.AutonCommand;
import org.simbotics.frcTemplate.auton.RobotComponent;
import org.simbotics.frcTemplate.io.RobotOutput;

public class DriveSetOutput extends AutonCommand {

	private double speed;
	private RobotOutput robotOut;

	public DriveSetOutput(double speed) {
		super(RobotComponent.DRIVE);
		this.speed = speed;

		this.robotOut = RobotOutput.getInstance();
	}

	@Override
	public void firstCycle() {
		// nothing!!!
	}

	@Override
	public boolean calculate() {
		this.robotOut.setDriveLeft(this.speed);
		this.robotOut.setDriveRight(this.speed);
		return true;

	}

	@Override
	public void override() {
		this.robotOut.setDriveLeft(0.0);
		this.robotOut.setDriveRight(0.0);
	}

}
