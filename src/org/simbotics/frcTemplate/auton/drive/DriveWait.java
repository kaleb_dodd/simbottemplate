package org.simbotics.frcTemplate.auton.drive;

import org.simbotics.frcTemplate.auton.AutonCommand;
import org.simbotics.frcTemplate.auton.RobotComponent;

public class DriveWait extends AutonCommand {

	public DriveWait() {
		super(RobotComponent.DRIVE);
	}

	@Override
	public boolean calculate() {
		return true;
	}

	@Override
	public void override() {
		// TODO Auto-generated method stub

	}

	@Override
	public void firstCycle() {
		// TODO Auto-generated method stub

	}

}
