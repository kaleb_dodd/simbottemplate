package org.simbotics.frcTemplate.auton.drive;

import org.simbotics.frcTemplate.RobotConstants;
import org.simbotics.frcTemplate.auton.AutonCommand;
import org.simbotics.frcTemplate.auton.RobotComponent;
import org.simbotics.frcTemplate.io.Dashboard;
import org.simbotics.frcTemplate.io.RobotOutput;
import org.simbotics.frcTemplate.io.SensorInput;
import org.simbotics.frcTemplate.util.SimLib;
import org.simbotics.frcTemplate.util.SimPID;
import org.simbotics.frcTemplate.util.SimPoint;

public class DriveToPoint extends AutonCommand {

	private double x;
	private double y;
	private double theta;
	private double minVelocity;
	private double maxVelocity;
	private double eps;
	private SensorInput sensorIn;
	private RobotOutput robotOut;
	private double turnRate;
	

	private SimPID turnPID;
	private SimPID straightPID;


	public DriveToPoint(double x, double y, double theta, long timeout) {
		this(x, y, theta, 0, 10, 0.25, 0, timeout);
	}

	public DriveToPoint(double x, double y, double theta, double minVelocity, double eps, long timeout) {
		this(x, y, theta, minVelocity, 11, 0, eps, timeout);
	}

	public DriveToPoint(double x, double y, double theta, double minVelocity, double maxVelocity, double eps,
			long timeout) {
		this(x, y, theta, minVelocity, maxVelocity, 0, eps, timeout);
	}

	public DriveToPoint(double x, double y, double theta, double minVelocity, double maxVelocity, double rampRate,
			double eps, long timeout) {
		super(RobotComponent.DRIVE, timeout);
		this.x = x;
		this.y = y;
		this.theta = theta;
		this.minVelocity = minVelocity;
		this.maxVelocity = maxVelocity;
		this.eps = eps;
		

		this.sensorIn = SensorInput.getInstance();
		this.robotOut = RobotOutput.getInstance();

	}

	private SimPoint getRotatedError() {
		double currentX = this.sensorIn.getDriveXPos();
		double currentY = this.sensorIn.getDriveYPos();
		double rotation = 90 - this.theta;

		SimPoint currentPosition = new SimPoint(currentX, currentY);
		SimPoint finalPosition = new SimPoint(this.x, this.y);

		currentPosition.rotateByAngleDegrees(rotation);
		finalPosition.rotateByAngleDegrees(rotation);

		double xError = finalPosition.getX() - currentPosition.getX();
		double yError = finalPosition.getY() - currentPosition.getY();

		return new SimPoint(xError, yError);

	}

	@Override
	public void firstCycle() {
		this.straightPID = new SimPID(RobotConstants.getDriveStraightPID());
		this.straightPID.setMinMaxOutput(this.minVelocity, this.maxVelocity);
		this.straightPID.setMinDoneCycles(1);
		this.turnPID = new SimPID(RobotConstants.getDriveTurnPID());
		this.turnPID.setMaxOutput(11);
		this.robotOut.configureDrivePID(RobotConstants.getTalonVelocityPID());
		this.turnRate = Dashboard.getInstance().getOPTurnRate();
	}

	@Override
	public boolean calculate() {
		SimPoint error = getRotatedError();
		double targetHeading;
		if (error.getY() < 0) {
			error.setX(-error.getX());
		}
		
		double turningOffset = (error.getX() * this.turnRate);
		
		if(turningOffset > 90) {
			turningOffset = 90;
		} else if(turningOffset < -90) {
			turningOffset = -90;
		}

		targetHeading = this.theta - turningOffset;

		double angle = this.sensorIn.getGyroAngle();
		double offset = angle % 360;

		if (targetHeading - offset < -180) {
			this.turnPID.setDesiredValue(angle + 360 + targetHeading - offset);
		} else if (targetHeading - offset < 180) {
			this.turnPID.setDesiredValue(angle + targetHeading - offset);
		} else {
			this.turnPID.setDesiredValue(angle - 360 + targetHeading - offset);
		}
		double yError = error.getY();
		double yOutput;
		
		yOutput = this.straightPID.calcPIDError(yError);			
		
		double distanceFromTargetHeading = Math.abs(this.turnPID.getDesiredVal() - this.sensorIn.getAngle());
		if (distanceFromTargetHeading > 90) {
			distanceFromTargetHeading = 90;
		}
		System.out.println("y output before scaling: " + yOutput);

		yOutput = yOutput * (((-1 * distanceFromTargetHeading) / 90.0) + 1);// Math.cos(Math.toRadians(distanceFromTargetHeading));

		double xOutput = -this.turnPID.calcPID(this.sensorIn.getAngle());

		double leftOut = SimLib.calcLeftTankDrive(xOutput, yOutput);
		double rightOut = SimLib.calcRightTankDrive(xOutput, yOutput);

		this.robotOut.setDriveLeftVelocity(leftOut);
		this.robotOut.setDriveRightVelocity(rightOut);

		System.out.println("y output after scaling: " + yOutput);

		double dist = (yError);
		if (Math.abs(dist) < this.eps) {
			System.out.println("I have reached the epsilon!");
		}
		boolean returning = Math.abs(dist) < this.eps;
		if (returning) {
			override();
		}

		return returning;

	}

	@Override
	public void override() {

		this.robotOut.setDriveLeft(0);
		this.robotOut.setDriveRight(0);

	}

}
