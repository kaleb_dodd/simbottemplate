package org.simbotics.frcTemplate.auton.drive;

import org.simbotics.frcTemplate.auton.AutonCommand;
import org.simbotics.frcTemplate.auton.RobotComponent;
import org.simbotics.frcTemplate.io.RobotOutput;
import org.simbotics.frcTemplate.io.SensorInput;
import org.simbotics.frcTemplate.util.SimPoint;

public class DriveSetPosition extends AutonCommand {

	private double x;
	private double y;
	private double angle;
	private SensorInput sensorIn;
	private RobotOutput robotOut;

	public DriveSetPosition(SimPoint p, double angle) {
		this(p.getX(), p.getY(), angle);
	}

	public DriveSetPosition(double x, double y, double angle) {
		super(RobotComponent.DRIVE);
		this.x = x;
		this.y = y;
		this.angle = angle;
		this.sensorIn = SensorInput.getInstance();
		this.robotOut = RobotOutput.getInstance();
	}

	@Override
	public void firstCycle() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean calculate() {
		this.sensorIn.setDriveXPos(this.x);
		this.sensorIn.setDriveYPos(this.y);
		this.sensorIn.setAutoStartAngle(this.angle);
		
		return true;
	}

	@Override
	public void override() {
		// TODO Auto-generated method stub

	}

}
