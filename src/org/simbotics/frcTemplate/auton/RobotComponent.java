package org.simbotics.frcTemplate.auton;

public enum RobotComponent {
	// GOTCHA: add auton components here
	UTIL, DRIVE, INTAKE, LIFT
}
